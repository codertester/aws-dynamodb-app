# README #

This README summaries the contents of this repository.

### What is this repository for? ###

* Create and delete AWS DynamoDB table and carry out data create, read and delete actions on AWS DynamoDB table.
* Version 1.0

### Created or deleted resource(s) ###

* AWS DynamoDB Table

### Implemented methods ###

* create_table()
* delete_table()
* put_data()
* get_data()
* delete_data()


### Repo usage ###

* Deployment instructions: deploy directly on server

### Contribution guidelines ###

* Create branch
* Add codes
* Submit pull request for code review

### Who do I talk to? ###

* Repo owner or admin

---
 * [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
